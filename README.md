# PruebaFrontEnd

Este proyecto esta generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

## Iniciar servidor

Se debe clonar el repositorio, correr el comando npm install en la carpeta del proyecto Correr `ng serve -o` para iniciar el servidor

## Especificaciones tecnicas

Se implemento el diseño segun especificaciones tecnicas de [diseño](https://xd.adobe.com/spec/db4d4481-94e5-49ad-459a-0b0b0adfaa30-ad7c/specs/)

## Instalaciones

La aplicacion consta de una instalacion de Bootstrap local con importacion en Angular.json, Firebase y el CDN
[GOV.CO](https://cdn.www.gov.co/v2/pages/inicio) necesario para la aplicacion

## Implementaciones

Se implemento del CDN [GOV.CO](https://cdn.www.gov.co/v2/pages/inicio) el header y el footer con algunas modificaciones necesarias para el diseño 

## Base de datos

Todos los datos de la aplicacion provienen de la base de datos de firebase, se crearon documentos necesarios para las pruebas

## Servicios 

La aplicacion consta con un servicio compartido con las transacciones basicas con firebase, los servicios de la aplicacion extienden del servicio compartido proporcionando el nombre de la coleccion necesario.

## Pipelines

La Aplicacion consta de un pipe para filtrar cualquier coleccion 

## modulos

Se implementa un modulo de tramites y servicios donde esta los componentes basicos del inicio de la aplicacion 

## Componentes

Se dividio la aplicacion en varios componentes, el componente home agrupa todos los componentes que hacen parte del inicio, cada componente tiene su parte de detalle donde se carga todos los items del componente seleccionado, el componente tramite detalle carga el tramite seleccionado, el componente tramite masivo hace una busqueda segun la letra ingresada, se comunica con el componente tramitesyservicios por parte del servicio el cual carga la palabra o letra.

## Modelos

La aplicacion consta del respectivo modelo para cada objeto necesario

## Formulario

Se realiza un formulario con un solo campo de opinion envia el comentario ingresado validando lo siguiente: es requerido, minimo de caracteres 5 y maximo 255 caracteres, cuando es valido el formulario se envia y se guarda en la base de datos de firebase


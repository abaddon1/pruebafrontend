import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformateDetalleComponent } from './informate-detalle.component';

describe('InformateDetalleComponent', () => {
  let component: InformateDetalleComponent;
  let fixture: ComponentFixture<InformateDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformateDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformateDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InformateService } from '../services/informate.service';

import * as fromModels from '../models';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente que carga todas las noticias */
/*=======================================================================*/
@Component({
   selector: 'app-informate-detalle',
   templateUrl: './informate-detalle.component.html',
   styleUrls: ['./informate-detalle.component.sass'],
})
export class InformateDetalleComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   noticias: Array<fromModels.Noticia>;
   constructor(private informateService: InformateService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.informateService.getAll().subscribe((result) => {
            this.noticias = [];
            result.docs.forEach((value) => {
               const noticia = new fromModels.Noticia(value.data());
               noticia.id = value.id;
               this.noticias.push(noticia);
            });
            this.noticias.sort((a, b) => 0 - (a.posicion < b.posicion ? 1 : -1));
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

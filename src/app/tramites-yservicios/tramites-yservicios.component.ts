import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TramitesService } from './services/tramite.service';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Contiene el envio de la palabra a buscar tramites a medida que se teclea */
/*=======================================================================*/

@Component({
   selector: 'app-tramites-yservicios',
   templateUrl: './tramites-yservicios.component.html',
   styleUrls: ['./tramites-yservicios.component.sass'],
})
export class TramitesYServiciosComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   search = new FormControl('');
   constructor(private tramitesService: TramitesService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.search.valueChanges.subscribe((value) => {
            this.tramitesService.searchWord(value);
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

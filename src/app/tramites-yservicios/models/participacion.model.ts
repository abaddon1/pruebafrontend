export class Participacion {
   id: string;
   readonly codigotiempo: number;
   readonly tiempo: string;
   readonly titulo: string;
   readonly ministerio: string;
   readonly cantidad: number;
   constructor(...args: Partial<Participacion>[]) {
      Object.assign(this, InitParticipacion, ...args);
   }
}
const InitParticipacion: Partial<Participacion> = {
   id: '',
   codigotiempo: 0,
   tiempo: '',
   titulo: '',
   ministerio: '',
   cantidad: 0,
};

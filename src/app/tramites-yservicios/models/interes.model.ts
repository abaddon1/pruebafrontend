export class Interes {
   id: string;
   readonly titulo: string;
   readonly mensaje: string;
   readonly nombreaccion: string;
   readonly link: string;
   readonly imagen: string;
   readonly posicion: string;
   constructor(...args: Partial<Interes>[]) {
      Object.assign(this, InitInteres, ...args);
   }
}
const InitInteres: Partial<Interes> = {
   id: '',
   titulo: '',
   mensaje: '',
   nombreaccion: '',
   link: '',
   imagen: '',
   posicion: '',
};

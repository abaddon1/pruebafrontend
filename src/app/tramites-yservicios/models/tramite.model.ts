export class Tramite {
   id: string;
   name: string;
   ministerio: string;
   medio: string;
   costo: string;
   detalle: string;
   constructor(...args: Partial<Tramite>[]) {
      Object.assign(this, InitTramite, ...args);
   }
}
const InitTramite: Partial<Tramite> = {
   id: '',
   name: '',
   ministerio: '',
   medio: '',
   costo: '',
   detalle: '',
};

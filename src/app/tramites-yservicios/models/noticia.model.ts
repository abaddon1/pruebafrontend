export class Noticia {
   id: string;
   readonly principal: boolean;
   readonly fecha: string;
   readonly mensaje: string;
   readonly imagen: string;
   readonly posicion: string;
   constructor(...args: Partial<Noticia>[]) {
      Object.assign(this, InitNoticia, ...args);
   }
}
const InitNoticia: Partial<Noticia> = {
   id: '',
   principal: false,
   fecha: '',
   mensaje: '',
   imagen: '',
   posicion: '',
};

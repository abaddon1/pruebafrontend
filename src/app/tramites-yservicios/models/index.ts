/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Exportacion de modelos de la aplicacion */
/*=======================================================================*/
export * from './interes.model';
export * from './noticia.model';
export * from './opinion.model';
export * from './tramite.model';
export * from './participacion.model';

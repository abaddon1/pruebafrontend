export class Opinion {
   readonly id: string;
   readonly mensaje: string;
   constructor(...args: Partial<Opinion>[]) {
      Object.assign(this, InitOpinion, ...args);
   }
}
const InitOpinion: Partial<Opinion> = {
   mensaje: '',
};

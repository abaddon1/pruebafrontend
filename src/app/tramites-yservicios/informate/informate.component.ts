import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import * as fromModels from '../models';
import { InformateService } from '../services/informate.service';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente basico de informate hace parte del home */
/*=======================================================================*/
@Component({
   selector: 'app-informate',
   templateUrl: './informate.component.html',
   styleUrls: ['./informate.component.sass'],
})
export class InformateComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   noticias: Array<fromModels.Noticia>;
   constructor(private informateService: InformateService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.informateService.getAll().subscribe((result) => {
            this.noticias = [];
            result.docs.forEach((value) => {
               const noticia = new fromModels.Noticia(value.data());
               noticia.id = value.id;
               this.noticias.push(noticia);
            });
            this.noticias.sort((a, b) => 0 - (a.posicion < b.posicion ? 1 : -1));
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

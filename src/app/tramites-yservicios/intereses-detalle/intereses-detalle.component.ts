import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InteresService } from '../services/interes.service';

import * as fromModels from '../models';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente que carga todos los intereses */
/*=======================================================================*/
@Component({
   selector: 'app-intereses-detalle',
   templateUrl: './intereses-detalle.component.html',
   styleUrls: ['./intereses-detalle.component.sass'],
})
export class InteresesDetalleComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   intereses: Array<fromModels.Interes>;
   constructor(private interesService: InteresService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.interesService.getAll().subscribe((result) => {
            this.intereses = [];
            result.docs.forEach((value) => {
               const interes = new fromModels.Interes(value.data());
               interes.id = value.id;
               this.intereses.push(interes);
            });
            this.intereses.sort((a, b) => 0 - (a.posicion < b.posicion ? 1 : -1));
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

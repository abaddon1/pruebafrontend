import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteresesDetalleComponent } from './intereses-detalle.component';

describe('InteresesDetalleComponent', () => {
  let component: InteresesDetalleComponent;
  let fixture: ComponentFixture<InteresesDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteresesDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteresesDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { SharedService } from '../../services/shared.service';

import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Servicio exclusivo de participacion que extiende del compartido */
/*=======================================================================*/
@Injectable({
    providedIn: 'root'
})
export class ParticipacionService extends SharedService {
    constructor(fireService: AngularFirestore) {
        super('participaciones', fireService);
     }
}
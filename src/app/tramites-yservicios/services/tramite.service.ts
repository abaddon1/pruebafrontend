import { Injectable } from '@angular/core';
import { SharedService } from '../../services/shared.service';

import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Subject } from 'rxjs';
/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Servicio exclusivo de tramites que extiende del compartido */
/* Implementa la necesidad de comunicacion con entre componentes hermanos */
/*=======================================================================*/
@Injectable({
   providedIn: 'root',
})
export class TramitesService extends SharedService {
   subject = new Subject<string>();
   resetSubject = this.subject.asObservable();
   
   constructor(fireService: AngularFirestore) {
      super('tramites', fireService);
   }
   searchWord(word: string): void {
      this.subject.next(word);
   }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ParticipacionService } from '../services/participacion.service';
import { OpinionService } from '../services/opinion.service';
import * as fromModels from '../models';
import { Subscription } from 'rxjs';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente basico de participacion hace parte del home */
/*=======================================================================*/
@Component({
   selector: 'app-participacion',
   templateUrl: './participacion.component.html',
   styleUrls: ['./participacion.component.sass'],
})
export class ParticipacionComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   formOpinion: FormGroup;
   lengthCtrl = 255;
   participaciones: Array<fromModels.Participacion>;
   alert = false;
   constructor(private opinionService: OpinionService, private participacionService: ParticipacionService) {}

   ngOnInit(): void {
      this.formOpinion = new FormGroup({
         mensaje: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      });
      this.subcriptions
         .add(
            this.formOpinion.get('mensaje').valueChanges.subscribe((value) => {
               this.lengthCtrl = 255 - value?.length || 255;
            })
         )
         .add(
            this.participacionService.getAll().subscribe((result) => {
               this.participaciones = [];
               result.docs.forEach((value) => {
                  const participacion = new fromModels.Participacion(value.data());
                  participacion.id = value.id;
                  this.participaciones.push(participacion);
               });
            })
         );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
   saveOpinion(): void {
      if (this.formOpinion.valid) {
         const opinion = new fromModels.Opinion(this.formOpinion.getRawValue());
         this.opinionService
            .addOne(opinion)
            .then((result) => {
               if (result) {
                  this.formOpinion.reset();
                  this.alert = true;
                  const interval = setInterval(() => {
                     this.alert = false;
                     clearInterval(interval);
                  }, 3000);
               }
            })
            .catch((error) => console.log('error: >>', error));
      }
   }
}

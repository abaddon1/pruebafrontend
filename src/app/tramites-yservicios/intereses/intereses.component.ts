import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import * as fromModels from '../models';
import { InteresService } from '../services/interes.service';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente basico de intereses hace parte del home */
/*=======================================================================*/
@Component({
   selector: 'app-intereses',
   templateUrl: './intereses.component.html',
   styleUrls: ['./intereses.component.sass'],
})
export class InteresesComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   intereses: Array<fromModels.Interes>;
   constructor(private interesService: InteresService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.interesService.getAll().subscribe((result) => {
            this.intereses = [];
            result.docs.forEach((value) => {
               const interes = new fromModels.Interes(value.data());
               interes.id = value.id;
               this.intereses.push(interes);
            });
            this.intereses.sort((a, b) => 0 - (a.posicion < b.posicion ? 1 : -1));
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

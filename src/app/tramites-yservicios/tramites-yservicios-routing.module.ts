import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HometramitesyserviciosComponent } from './hometramitesyservicios/hometramitesyservicios.component';
import { InformateDetalleComponent } from './informate-detalle/informate-detalle.component';
import { InteresesDetalleComponent } from './intereses-detalle/intereses-detalle.component';
import { ParticipacionDetalleComponent } from './participacion-detalle/participacion-detalle.component';
import { TramitesDetalleComponent } from './tramites-detalle/tramites-detalle.component';
import { TramitesMasivoComponent } from './tramites-masivo/tramites-masivo.component';
import { TramitesYServiciosComponent } from './tramites-yservicios.component';

/* ======================================================================= */
/* Declaracion de las rutas del modulo tramites y servicios */
/* ======================================================================= */
const routes: Routes = [
   {
      path: '',
      component: TramitesYServiciosComponent,
      children: [
         { path: '', redirectTo: 'home', pathMatch: 'full' },
         { path: 'home', component: HometramitesyserviciosComponent },
         { path: 'tramitesdetalle/:id', component: TramitesDetalleComponent },
         { path: 'tramitesmasivo', component: TramitesMasivoComponent },
         { path: 'participaciondetalle', component: ParticipacionDetalleComponent },
         { path: 'interesesdetalle', component: InteresesDetalleComponent },
         { path: 'informatedetalle', component: InformateDetalleComponent },
      ],
   },
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule],
})
export class TramitesYServiciosRoutingModule {}

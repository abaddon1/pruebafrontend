import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TramitesYServiciosRoutingModule } from './tramites-yservicios-routing.module';
import { TramitesYServiciosComponent } from './tramites-yservicios.component';
import { TramitesComponent } from './tramites/tramites.component';
import { ParticipacionComponent } from './participacion/participacion.component';
import { InformateComponent } from './informate/informate.component';
import { InteresesComponent } from './intereses/intereses.component';
import { HometramitesyserviciosComponent } from './hometramitesyservicios/hometramitesyservicios.component';
import { TramitesDetalleComponent } from './tramites-detalle/tramites-detalle.component';
import { ParticipacionDetalleComponent } from './participacion-detalle/participacion-detalle.component';
import { InteresesDetalleComponent } from './intereses-detalle/intereses-detalle.component';
import { InformateDetalleComponent } from './informate-detalle/informate-detalle.component';
import { TramitesMasivoComponent } from './tramites-masivo/tramites-masivo.component';
import { FilterCollectionPipe } from '../pipes/filter-collection.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TramitesService } from './services/tramite.service';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Declaracion de componentes del modulo de tramites y servicios */
/*=======================================================================*/

@NgModule({
   declarations: [
      TramitesYServiciosComponent,
      TramitesComponent,
      ParticipacionComponent,
      InformateComponent,
      InteresesComponent,
      HometramitesyserviciosComponent,
      TramitesDetalleComponent,
      ParticipacionDetalleComponent,
      InteresesDetalleComponent,
      InformateDetalleComponent,
      TramitesMasivoComponent,
      FilterCollectionPipe,
   ],
   imports: [CommonModule, TramitesYServiciosRoutingModule, FormsModule, ReactiveFormsModule],
   providers: [TramitesService],
})
export class TramitesYServiciosModule {}

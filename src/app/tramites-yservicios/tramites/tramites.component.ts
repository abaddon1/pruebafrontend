import { Component, OnDestroy, OnInit } from '@angular/core';
import { TramitesService } from '../services/tramite.service';
import * as fromModels from '../models';
import { Subscription } from 'rxjs';
/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente basico de tramites hace parte del home */
/*=======================================================================*/
@Component({
   selector: 'app-tramites',
   templateUrl: './tramites.component.html',
   styleUrls: ['./tramites.component.sass'],
})
export class TramitesComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   tramites: Array<fromModels.Tramite>;
   masUsados: Array<fromModels.Tramite>;
   basicos: Array<fromModels.Tramite>;
   constructor(private tramitesService: TramitesService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.tramitesService.getAll().subscribe((result) => {
            this.tramites = [];
            result.docs.forEach((value) => {
               const tramite = new fromModels.Tramite(value.data());
               tramite.id = value.id;
               this.tramites.push(tramite);
            });
            if (this.tramites.length > 0) {
               this.masUsados = this.tramites.filter((tramite) => tramite.costo === '');
               this.basicos = this.tramites.filter((tramite) => tramite.costo !== '');
            }
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
   trackbyFunction = (i, item) => (item ? item.id : undefined);
}

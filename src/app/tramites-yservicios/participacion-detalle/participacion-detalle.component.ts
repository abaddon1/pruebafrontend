import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import * as fromModels from '../models';
import { ParticipacionService } from '../services/participacion.service';

/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Componente que carga todas las participaciones */
/*=======================================================================*/
@Component({
   selector: 'app-participacion-detalle',
   templateUrl: './participacion-detalle.component.html',
   styleUrls: ['./participacion-detalle.component.sass'],
})
export class ParticipacionDetalleComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   participaciones: Array<fromModels.Participacion>;

   constructor(private participacionService: ParticipacionService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.participacionService.getAll().subscribe((result) => {
            this.participaciones = [];
            result.docs.forEach((value) => {
               const participacion = new fromModels.Participacion(value.data());
               participacion.id = value.id;
               this.participaciones.push(participacion);
            });
         })
      );
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

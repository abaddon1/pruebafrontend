import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionDetalleComponent } from './participacion-detalle.component';

describe('ParticipacionDetalleComponent', () => {
  let component: ParticipacionDetalleComponent;
  let fixture: ComponentFixture<ParticipacionDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

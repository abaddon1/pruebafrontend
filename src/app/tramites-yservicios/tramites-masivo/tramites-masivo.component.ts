import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import * as fromModels from '../models';
import { TramitesService } from '../services/tramite.service';
/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Carga todos lo tramites de la base de datos */
/* se puede realizar busquedas con el input inicial */
/*=======================================================================*/
@Component({
   selector: 'app-tramites-masivo',
   templateUrl: './tramites-masivo.component.html',
   styleUrls: ['./tramites-masivo.component.sass'],
})
export class TramitesMasivoComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   tramites: Array<fromModels.Tramite>;
   word: string;
   constructor(private tramitesService: TramitesService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.tramitesService.getAll().subscribe((result) => {
            this.tramites = [];
            result.docs.forEach((value) => {
               const tramite = new fromModels.Tramite(value.data());
               tramite.id = value.id;
               this.tramites.push(tramite);
            });
         })
      );
      /* Subcripcion al cambio de palabra de la busqueda */
      this.tramitesService.resetSubject.subscribe((value) => (this.word = value));
   }
   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
   trackbyFunction = (i, item) => (item ? item.id : undefined);
}

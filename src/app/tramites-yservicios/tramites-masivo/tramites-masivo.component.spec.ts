import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesMasivoComponent } from './tramites-masivo.component';

describe('TramitesMasivoComponent', () => {
  let component: TramitesMasivoComponent;
  let fixture: ComponentFixture<TramitesMasivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramitesMasivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesMasivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

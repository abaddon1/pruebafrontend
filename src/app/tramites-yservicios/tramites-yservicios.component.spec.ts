import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesYServiciosComponent } from './tramites-yservicios.component';

describe('TramitesYServiciosComponent', () => {
  let component: TramitesYServiciosComponent;
  let fixture: ComponentFixture<TramitesYServiciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramitesYServiciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesYServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

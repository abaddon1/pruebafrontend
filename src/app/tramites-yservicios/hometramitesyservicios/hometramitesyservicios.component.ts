import { Component, OnInit } from '@angular/core';
/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Agrupa los componentes basicos que hacen parte del modulo de tramites y servicios */
/*=======================================================================*/
@Component({
  selector: 'app-hometramitesyservicios',
  templateUrl: './hometramitesyservicios.component.html',
  styleUrls: ['./hometramitesyservicios.component.sass']
})
export class HometramitesyserviciosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HometramitesyserviciosComponent } from './hometramitesyservicios.component';

describe('HometramitesyserviciosComponent', () => {
  let component: HometramitesyserviciosComponent;
  let fixture: ComponentFixture<HometramitesyserviciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HometramitesyserviciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HometramitesyserviciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

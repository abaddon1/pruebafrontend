import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TramitesDetalleComponent } from './tramites-detalle.component';

describe('TramitesDetalleComponent', () => {
  let component: TramitesDetalleComponent;
  let fixture: ComponentFixture<TramitesDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramitesDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TramitesDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

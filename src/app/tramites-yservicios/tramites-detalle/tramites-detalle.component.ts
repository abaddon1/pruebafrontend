import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { TramitesService } from '../services/tramite.service';

import * as fromModels from '../models';
/*=======================================================================*/
/* Author: Oscar Eliecer Perez Aya */
/* Descripcion: Carga el detalle del tramite seleccionado mostrandolo  */
/*=======================================================================*/
@Component({
   selector: 'app-tramites-detalle',
   templateUrl: './tramites-detalle.component.html',
   styleUrls: ['./tramites-detalle.component.sass'],
})
export class TramitesDetalleComponent implements OnInit, OnDestroy {
   subcriptions: Subscription = new Subscription();
   id: string;
   tramite: fromModels.Tramite;
   constructor(private route: ActivatedRoute, private tramitesService: TramitesService) {}

   ngOnInit(): void {
      this.subcriptions.add(
         this.route.params.subscribe((params) => {
            this.id = params.id;
            this.subcriptions.add(
               this.tramitesService.getOne(this.id).subscribe((result) => {
                  this.tramite = new fromModels.Tramite(result.data());
                  this.tramite.id = this.id;
               })
            );
         })
      );
   }

   ngOnDestroy(): void {
      this.subcriptions.unsubscribe();
   }
}

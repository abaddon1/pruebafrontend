import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/* ======================================================================= */
/* Declaracion de rutas de la aplicacion se implementa con carga lenta */
/* ======================================================================= */
const routes: Routes = [
   { path: '', redirectTo: 'tramitesyservicios', pathMatch: 'full' },
   {
      path: 'tramitesyservicios',
      loadChildren: () => import('./tramites-yservicios/tramites-yservicios.module').then((x) => x.TramitesYServiciosModule),
   },
];

@NgModule({
   imports: [RouterModule.forRoot(routes, { useHash: true })],
   exports: [RouterModule],
})
export class AppRoutingModule {}

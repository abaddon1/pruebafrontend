import { Pipe, PipeTransform } from '@angular/core';

/* ======================================================================= */
/* Pipeline declarado para el filtro de colecciones de la aplicacion */
/* ======================================================================= */
@Pipe({
   name: 'filterCollection',
})
export class FilterCollectionPipe implements PipeTransform {
   transform(listData: any[], valueSearch?: any): any {
      if (!valueSearch) {
         return listData;
      }
      if (listData) {
         return listData.filter((x) => JSON.stringify(x).includes(valueSearch));
      }
   }
}

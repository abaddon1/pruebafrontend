import { Injectable } from '@angular/core';

import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';

/* ======================================================================= */
/* Declaracion de servicio global para la aplicacion se debe de extender   */
/* el servicio contiene la transacciones basicas para cualquier aplicacion */
/* se debe tener encuenta que el servicio solo trabaja con Firebase con sus */
/* especificaciones */
/* ======================================================================= */
@Injectable({
   providedIn: 'root',
})
export class SharedService {
   constructor(public controlador: string, private fireService: AngularFirestore) {}

   getAll(): Observable<firebase.firestore.QuerySnapshot> {
      return this.fireService.collection(this.controlador).get();
   }
   getOne(id: any): Observable<any> {
      return this.fireService.collection(this.controlador).doc(id).get();
   }
   addOne(item: any): Promise<any> {
      return this.fireService.collection(this.controlador).add({ ...item });
   }
   updateOne(item: any): Promise<any> {
      return this.fireService.collection(this.controlador).doc(item.id).update(item);
   }
   removeOne(id: any): Promise<any> {
      return this.fireService.collection(this.controlador).doc(id).delete();
   }
}

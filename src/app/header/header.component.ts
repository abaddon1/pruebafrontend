import { Component, OnInit } from '@angular/core';

@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
   navslinks = [
      { name: 'Trámites y servicios', link: '#tramites' },
      { name: 'Participación', link: '#participacion' },
      { name: 'Entidades', link: '#entidades' },
      { name: 'Noticias', link: '#noticias' },
   ];
   constructor() {}

   ngOnInit(): void {}
   btnSearchGovCo() {}
   changeLang() {}
}

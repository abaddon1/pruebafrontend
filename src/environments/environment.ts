// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


/* ======================================================================= */
/* Credenciales Firebase */
/* ======================================================================= */
export const environment = {
   production: false,
   firebase: {
      apiKey: 'AIzaSyDy_Q6JrgNDFLS708IUjZi8iylEMnsvMNg',
      authDomain: 'pruebafrontend-f7cd2.firebaseapp.com',
      databaseURL: 'https://pruebafrontend-f7cd2.firebaseio.com',
      projectId: 'pruebafrontend-f7cd2',
      storageBucket: 'pruebafrontend-f7cd2.appspot.com',
      messagingSenderId: '330385469377',
      appId: '1:330385469377:web:6c105019996384f0614940',
   },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
